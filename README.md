# CUTE 2022 Workshop
## Title / Titre

(EN)

Discover, ideate, and create mulsemedia materials

(FR)

Découvrir, idéer et créer des matériaux mulsemedia

## Presenter / Présentateur

[Christian Frisson](https://frisson.re)

## Abstract / Résumé

(EN)

(10 lines)

The goal of this workshop is to discover, ideate, and create mulsemedia materials. 
Mulsemedia materials are interactive artifacts produced with authoring tools, rendering multisensory feedback (for this workshop: visual, auditive, haptic) to improve the experience of people who create multimedia content. 
We will combine open-source tools created at the [Metalab](https://sat.qc.ca/fr/recherche/metalab) at the Society for Art and Technology (SAT), at the Input Devices and Music Interaction Lab ([IDMIL](http://idmil.org)) at McGill University, and at the [numediart](https://numediart.org) Institute at the University of Mons (UMONS).

Schedule:
* (20 min) Presentation: challenges and opportunities of mulsemedia materials
* (30 min) Presentation: hardware tools (with toolboxes available in groups)
   * (15 min) [LivePose](https://gitlab.com/sat-metalab/livepose/): an open-source tool for democratizing pose and action detection for multimedia arts and hybrid presence applications on open edge devices (NVIDIA Jetson)
   * (15 min) [ForceHost](https://gitlab.com/ForceHost): an open-source toolchain for generating firmware embedding the authoring and rendering of audio and force-feedback haptics on ESP32 platforms
* (10 min) Break
* (30 min) Presentation: software tools 
   * (15 min) [MediaCycle](https://github.com/MediaCycle): an open-source framework for organizing, visualizing and interacting with multimedia collections by content-based similarity
   * (15 min) [libmapper/webmapper](https://github.com/libmapper/): an open-source framework (library and Web UI) for connecting things, including the above-mentioned tools, by mapping multimodal signals
* (10 min) Break
* (20 min) Ideation: with office supplies: pen, paper, whiteboards, markers... (together in groups)
* (20 min) Presentation: pitch ideas and re-cluster groups
* (10 min) Break
* (40 min) Creation: with hardware and software tools (together in groups)
* (20 min) Presentation: exploration of demos 

Benefits for the participants: 
* discover challenges and opportunities of mulsemedia materials
* manipulate open-source tools supporting the design, production and rendering of mulsemedia materials
* learn designed-centered ideation and presentation techniques
* create and showcase prototypes in groups

(FR)

(10 lignes)

L'objectif de cet atelier est de découvrir, idéer, et créer des matériaux mulsemedia. 
Les matériaux mulsemedia sont des artéfacts interactifs produits avec des outils d'auteurisation, générant un rendu multisensoriel (pour cet atelier: visuel, auditif, haptique) pour améliorer l'expérience des personnes qui créent du contenu multimedia. 
Nous combinerons des outils à code source ouvert créés notamment au [Metalab](https://sat.qc.ca/fr/recherche/metalab) de la Société des Arts Technologiques (SAT), à l'Input Devices and Music Interaction Lab ([IDMIL](http://idmil.org)) de l'Université McGill, et à l'Institut [numediart](https://numediart.org) de l'Université de Mons (UMONS).

Agenda:
* (20 min) Présentation: enjeux et opportunités pour les matériaux mulsemedia
* (30 min) Présentation: outils matériels (avec kits disponibles en groupes)
   * (15 min) [LivePose](https://gitlab.com/sat-metalab/livepose/): un outil à code source ouvert pour démocratiser la détection de poses et d'action pour les arts 
   * (15 min) [ForceHost](https://gitlab.com/ForceHost): une solution à code source ouvert pour générer des micrologiciels embarquant l'auteurisation et le rendu audio et haptique à retour de force sur des plateformes ESP32
* (10 min) Pause
* (30 min) Présentation: outils logiciels
   * (15 min) [MediaCycle](https://github.com/MediaCycle): un cadriciel à code source ouvert pour organiser, visualiser et interagir avec des collections multimedia par similarité
   * (15 min) [libmapper/webmapper](https://github.com/libmapper/): un cadriciel à code source ouvert (librairie et Web UI) pour connecter des outils, incluant ceux mentionnés ci-dessus, par le mappage de signaux multimodaux
* (10 min) Pause
* (20 min) Idéation: avec fournitures de bureau: crayons, papier, tableaux, feutres... (ensemble en groupes)
* (20 min) Présentation: partage des idées et re-groupement
* (10 min) Pause
* (40 min) Création: avec outils matériels et logiciels (ensemble en groupes)
* (20 min) Présentation: exploration des démos 

Bénéfices pour les participant-e-s: 
* découvrir les matériaux mulsemedia, leurs enjeux et opportunités
* manipuler des outils à code source ouvert couvrant la conception, la production et le rendu de matériaux mulsemedia
* apprendre les techniques d'idéation et de présentation centrées sur la conception
* créer et montrer des prototypes en groupes

## Duration / Durée

3h30

## Diapos / Slides

Source: [slides.md](slides.md)

### Requirements

- Install Node.js and NPM.
- Install dependencies (once or after upgrades):
```
npm i
```

### Development

Write markdown while [reveal-md](https://github.com/webpro/reveal-md)'s local server converts to html slides.

- Run the local server:
```
npm run dev
```

### Release

- Convert md to html:
```
npm run html
```
- (Optional) Convert md to pdf:
```
npm run pdf
```
- Publish to github pages (gitlab pages automated with [continuous integration](.gitlab-ci.yml))
```
npm run publish
```

### Controls

- Use `Ctrl + click` to zoom on slides, useful for images
- Press `o` to show the slide overview
- Press `s` to open the speaker notes window (requires allowing popups)

Check the [reveal.js](https://github.com/hakimel/reveal.js/#speaker-notes) README for more tips.
