---
title: "Atelier Mulsemedia | CUTE 2022"
theme: white
separator: <!--section-->
verticalSeparator: <!--slide-->

revealOptions:
  transition: "none"
  slideNumber: "h.v"
  hashOneBasedIndex: true
  history: true
  margin: 0.01
  controls: true
  progress: true
  dependencies:
    - src: "module/reveal.js-plugins/chart/Chart.min.js"
    - src: "module/reveal.js-plugins/chart/csv2chart.js"
    - src: "module/reveald3/reveald3.js"
    - src: "module/reveal.js-plugins/spreadsheet/ruleJS.all.full.min.js"
    - src: "module/reveal.js-plugins/spreadsheet/spreadsheet.js"
    - src: "module/reveal-a11y/accessibility/helper.js"
---

<!-- Use this to add a background image: -->
<!-- .slide: class="" data-background="" id="start" data-state="start"-->

<!-- Use this transparent styling in case of a background image: -->
<div class="title">

<h2>Atelier Mulsemedia</h2>

### [CUTE 2022](https://www.eventbrite.com/e/cute-2022-numediart-masterclass-on-culture-and-technology-tickets-348187838197)

<!-- <div class="affiliations"> -->
<!-- <div class="affiliation"> -->
<div class="event">
<a class="logo" href="https://www.eventbrite.com/e/cute-2022-numediart-masterclass-on-culture-and-technology-tickets-348187838197" title="CUTE 2022"><img src="images/logos/cute-logo.png" alt="CUTE 2022"/></a>
</div>
<!-- </div> -->

<br/>

<!-- </div>  -->

<div class="authors">

<div class="author">
<img src="images/portraits/ChristianFrisson.jpg" alt="Christian Frisson"/>
<div class="name">
<a href="mailto:christian@frisson.re" title="Christian Frisson's Email">Christian</a></span> <a href="https://frisson.re" title="Christian Frisson's Website">Frisson</a></span>
</div>
</div>

</div>

<br/>

<div class="affiliations">
<div class="affiliation">
<a class="logo" href="https://sat.qc.ca" title="Société des Arts Technologiques"><img src="images/logos/SAT.png" alt="Société des Arts Technologiques"/></a>
<a class="logo" href="https://sat-metalab.gitlab.io" title="Metalab"><img src="images/logos/Metalab_Logo_Noir.png" alt="Metalab"/></a>
</div>
</div>

#### 2022-07-08

</div class="title">

<!--section-->

<!-- .slide: data-background-iframe="https://player.vimeo.com/video/390637480?h=32ecf7d883" data-background-video-loop="1" data-background-interactive -->

<!--slide-->

## Société des Arts Technologiques

<figure>
<img alt="images/3rdparty/SAT/sat_urbania_2.png" src="images/3rdparty/SAT/sat_urbania_2.png" />
<figcaption>
<a href="https://sat.qc.ca/fr/la-sat#section">https://sat.qc.ca/fr/la-sat#section</a>
</figcaption>
</figure>

<!--slide-->

## Metalab

<div class="affiliations">
<div class="affiliation">
<a class="logo" href="https://sat.qc.ca" title="Société des Arts Technologiques"><img src="images/logos/SAT.png" alt="Société des Arts Technologiques"/></a>
<a class="logo" href="https://sat-metalab.gitlab.io" title="Metalab"><img src="images/logos/Metalab_Logo_Noir.png" alt="Metalab"/></a>
</div>
</div>

<figure>
<img alt="images/projects/Metalab/logiciels_thematiques.svg" src="images/projects/Metalab/logiciels_thematiques.svg" />
<figcaption>
<a href="https://sat-metalab.gitlab.io/">https://sat-metalab.gitlab.io/</a>
</figcaption>
</figure>


<!--slide-->

## Parcours
### Doctorat 

<div class="fragment">

<h4>Sciences de l'ingénieur (2015)</h4>

##### Visualisation d'informations multimedia

<div class="affiliations">
<div class="affiliation">
<p><a class="logo" href="http://web.umons.ac.be" title="University of Mons"><img src="images/logos/umons-prelude.svg" alt="UMONS"/></a> Université de Mons, Belgique</p>
<p><a class="logo" href="http://numediart.org" title="numediart Institute for New Media Art Technology"><img src="images/logos/numediart.svg" alt="numediart"/></a> Institut pour les technologies créatives</p>

</div>
</div>

</div>

Notes:
- Pendant ma thèse de doctorat à l'Université de Mons, j'ai exploré comment améliorer l'interaction et la visualisation pour la navigation dans des collections multimedia, un sujet lié aux sciences de l'information



<!--slide-->

## Parcours


<h3>Postdoctorats</h3>


<div class="fragment">

<h4>Interaction humain-machine (2016-2017)</h4>

<div class="affiliations">
<div class="affiliation">
<p>
<a class="logo" href="https://www.inria.fr" title="Inria"><img src="images/logos/inr_logo_rouge.svg" alt="Inria"/></a>
<!-- Inria -->
Lille Nord-Europe, France
<!-- Interaction humain-machine (haptique) -->
<br/>
financé par le projet EU H2020 HAPPINESS
<!-- <a class="logo" href="http://mjolnir.lille.inria.fr" title="Mjolnir team"><img src="images/logos/mjolnir-logo.png" alt="Mjolnir"/></a> -->
</p>
</div>
</div>

</div>
<div class="fragment">

<h4>Visualisation d'information (2017-2018)</h4>

<div class="affiliations">
<div class="affiliation">
<p>
<a class="logo" href="https://ucalgary.ca" title="University of Calgary"><img src="images/logos/uc-horz-rgb.png" alt="University of Calgary"/></a>
<!-- Université de Calgary -->
Alberta, Canada
<!-- Visualisation d'information -->
<br/>
financé par le projet EnergyVis avec la Régie d'Énergie du Canada
<!-- <a class="logo" href="http://ilab.cpsc.ucalgary.ca" title="Interactions Lab"><img src="images/logos/ilab-logo.png" alt="Interactions Lab"/></a> -->
</p>
</div>
</div>

</div>
<div class="fragment">

<h4>Interaction humain-machine & Visualisation d'information (2019-2020)</h4>

<div class="affiliations">
<div class="affiliation">
<p>
<a class="logo" href="https://mcgill.ca" title="McGill University"><img src="images/logos/mcgill_sig_red.png" alt="McGill University"/></a>
<!-- Université McGill -->
Québec, Canada
<!-- Interaction humain-machine (haptique) et Visualisation d'information -->
<br/>
<!-- <a class="logo" href="https://www.mitacs.ca" title="Mitacs"><img src="images/logos/MitacsTwitter.png" alt="Mitacs"/></a> -->
financé par ma bourse Mitacs Élévation avec Haply Robotique
<!-- <a class="logo" href="http://idmil.org" title="Input Devices and Music Interaction Lab"><img src="images/logos/IDMIL-cropped-sq-rnd.png" alt="IDMIL"/></a> -->
</p>
</div>
</div>

</div>



Notes:
- J'ai une expérience internationale et diversifiée de recherche postdoctorale:
- à l'Université McGill, financé par la bourse Mitacs Elévation que j'ai obtenu, 
- à l'Université de Calgary, en collaboration avec la Régie de l'Énergie du Canada,
- à l'Inria en France, sur le projet Européen Horizon 2020 HAPPINESS.

<!--section-->

## Agenda

* (5 min) **Intro**
* (15 min) **Présentation**: enjeux et opportunités pour les matériaux mulsemedia
* (30 min) **Présentation**: outils matériels (avec kits disponibles en groupes)
   * (15 min) [LivePose](https://gitlab.com/sat-metalab/livepose/): un outil à code source ouvert pour démocratiser la détection de poses et d'action pour les arts 
   * (15 min) [ForceHost](https://gitlab.com/ForceHost): une solution à code source ouvert pour générer des micrologiciels embarquant l'auteurisation et le rendu audio et haptique à retour de force sur des plateformes ESP32
* (10 min) **Pause**
* (30 min) **Présentation**: outils logiciels
   * (15 min) [MediaCycle](https://github.com/MediaCycle): un cadriciel à code source ouvert pour organiser, visualiser et interagir avec des collections multimedia par similarité
   * (15 min) [libmapper/webmapper](https://github.com/libmapper/): un cadriciel à code source ouvert (librairie et Web UI) pour connecter des outils, incluant ceux présentés, par le mappage de signaux multimodaux
* (10 min) **Pause**
* (20 min) **Idéation**: avec fournitures de bureau: crayons, papier, tableaux, feutres... (ensemble en groupes)
* (20 min) **Présentation**: partage des idées et re-groupement
* (10 min) **Pause**
* (40 min) **Création**: avec outils matériels et logiciels (ensemble en groupes)
* (20 min) **Présentation**: exploration des démos 

<!--slide-->

## Agenda: Redux

* (5 min) **Intro**
* (15 min) **Présentation**: enjeux et opportunités pour les matériaux mulsemedia
* (30 min) **Présentation**: outils matériels (avec kits disponibles en groupes)
   * (15 min) [LivePose](https://gitlab.com/sat-metalab/livepose/): un outil à code source ouvert pour démocratiser la détection de poses et d'action pour les arts 
   * (15 min) [ForceHost](https://gitlab.com/ForceHost): une solution à code source ouvert pour générer des micrologiciels embarquant l'auteurisation et le rendu audio et haptique à retour de force sur des plateformes ESP32
* (10 min) **Pause**
* (30 min) **Présentation**: outils logiciels
   * (15 min) [MediaCycle](https://github.com/MediaCycle): un cadriciel à code source ouvert pour organiser, visualiser et interagir avec des collections multimedia par similarité
   * (15 min) [libmapper/webmapper](https://github.com/libmapper/): un cadriciel à code source ouvert (librairie et Web UI) pour connecter des outils, incluant ceux présentés, par le mappage de signaux multimodaux
* (10 min) **Pause**

<span style="color:gray">

* ~~(20 min) **Idéation**: avec fournitures de bureau: crayons, papier, tableaux, feutres... (ensemble en groupes)~~
* ~~(20 min) **Présentation**: partage des idées et re-groupement~~
* ~~(10 min) **Pause**~~
* ~~(40 min) **Création**: avec outils matériels et logiciels (ensemble en groupes)~~

</span>

* (20 min) **Présentation**: exploration des démos 


<!--slide-->

## Bénéfices pour les participant-e-s

* découvrir les matériaux mulsemedia, leurs enjeux et opportunités
* manipuler des outils à code source ouvert couvrant la conception, la production et le rendu de matériaux mulsemedia

<span style="color:gray">

* ~~apprendre les techniques d'idéation et de présentation centrées sur la conception~~
* ~~créer et montrer des prototypes en groupes~~

</span>

<!--section-->

## Présentation

### Enjeux et opportunités pour les matériaux mulsemedia

(10 min)

<!--slide-->

## Combinons des domaines de recherche

<!-- <img src="images/venn.svg"/> -->

<svg width="432.53268" height="405.2327">
   <g class="fragment" data-fragment-index="1">
      <circle style="fill:none;stroke:#1f78b4;stroke-width:5" id="path910-9" cx="216.81494" cy="142.36723" r="140" />
      <text x="155" y="64" font-size="55%">Visualisation</text>
      <text x="150" y="84" font-size="55%">d'Informations</text>
   </g>
   <g class="fragment" data-fragment-index="2">
      <circle style="fill:none;stroke:#a6cee3;stroke-width:5" id="path910" cx="142.3401" cy="262.86545" r="140" />
      <text x="15" y="285" font-size="55%">Information</text>
      <text x="15" y="305" font-size="55%">(Multimedia)</text>
   </g>
   <g class="fragment" data-fragment-index="3">
      <circle style="fill:none;stroke:#b2df8a;stroke-width:5" id="path910-6" cx="290.19257" cy="262.48398" r="140" />
      <text x="295" y="285" font-size="55%">Interaction</text>
      <text x="295" y="305" font-size="55%">(Haptique)</text>
   </g>
</svg>

<!--slide-->
## Haptique?

<div class="figures">
<div class="figure">
<img class="thumb" alt="" src="images/3rdparty/HapticFeltAttributes.png">
<div class="cite"><a href="http://www.cherylaknerkoler.com">Cheryl Akner Koler</a>, 2016</div>
<!-- http://www.cherylaknerkoler.com/research/Entries/2016/11/1_HAPTICA.html -->
</div>
</div>

Note:
* Haptics stimulate the sense of touch, or senses in plural, as touch can be described through many physiological effects tied to physical properties, as illustrated in this figure.
* These physical properties and the physiological effects they trigger are dimension among many to categorize haptic displays.

<!--slide-->

## Interaction classique

<!-- ### Enjeux -->
<!-- ### Informations, visualisation et interaction -->
<!-- <h3>
<h3><span class="mir">information</span> <span class="vis">visualisation</span> <span class="hci">interaction</span></h3>
</h3> -->

<div class="row">
<div class="col-50 fragment">

> Les <span class="hci">interactions</span> avec des <span class="vis">visualisations</span> de <span class="mir">données</span> sont contraintes par l’écran, le clavier et le pointeur.

</div>
<div class="col-50">

<div class="figure">
<img class="thumb" alt="InfoPhys Poster" src="images/projects/2020-03-05-IDMIL-Haptic-Showroom-Closeup.jpg"/>
<div class="cite">
<b>Christian Frisson</b>, Colin Gallacher,  Marcelo M. Wanderley. “<a href="https://hal.inria.fr/hal-02050235">Haptic Techniques for Browsing Sound Maps Organized by Similarity</a>”, 9th Intl. Workshop on Haptic and Audio Interaction Design [HAID’19]
</div>
</div>

</div>
</div>

<!--slide-->

## Interaction tactile

<!-- ### Informations, visualisation et interaction -->
<!-- <h3>
<h3><span class="mir">information</span> <span class="vis">visualisation</span> <span class="hci">interaction</span></h3>
</h3> -->

<div class="row">
<div class="col-50">

<div class="fragment">

> Les <span class="vis">écrans</span> <span class="hci">tactiles</span> permettent à une fraction de la population d’interagir avec **plusieurs doigts sur des millions de pixels**.

</div>
<div class="fragment">

> Mais produisent un **retour pas très stimulant pour notre sens du toucher**.

</div>

</div>
<div class="col-50">

<div class="figures">
<div class="figure">
<!-- <img class="thumb" alt="EnergyVisCHI2018EA Vignette" src="images/projects/EnergyVis/EnergyVisCHI2018EA.jpg"/> -->
  <video autoplay loop muted>
    <source data-src="videos/projects/EnergyVis/NEB-00.00.09.680-00.00.14.987.mp4" type="video/mp4" />
  </video>
<div class="cite">
Knudsen, Søren, Jo Vermeulen, Doris Kosminsky, Jagoda Walny, Mieka West, <b>Christian Frisson</b>, Bon Adriel Aseniero, Lindsay MacDonald Vermeulen, Charles Perin, Lien Quach, Peter Buk, Katrina Tabuli, Shreya Chopra, Wesley Willett, Sheelagh Carpendale, “<a href="https://doi.org/10.1145/3170427.3186539">Democratizing Open Energy Data for Public Discourse Using Visualization</a>.” 35th Conference Extended Abstracts on Human Factors in Computing Systems [ACM CHI’18 EA]
</div>

</div>
</div>

<!--slide-->

## Interaction haptique

<!-- ### Informations, visualisation et interaction -->
<!-- <h3><span class="mir">information</span> <span class="vis">visualisation</span> <span class="hci">interaction</span></h3> -->

<div class="row">
<div class="col-50">

<div class="fragment">

> Les <span class="vis">interfaces</span> <span class="hci">haptiques</span> à retour d’effort permettent de **simuler des stimulations du toucher**.

</div>
<div class="fragment">

> Mais fournissent généralement qu’**un seul pointeur pour la manipulation**.

</div>

</div>
<div class="col-50">

<div class="figures">
<div class="figure">
<!-- <img class="thumb" alt="InfoPhys Vignette" src="images/projects/InfoPhys.jpg"/> -->
  <video autoplay loop muted>
    <source data-src="videos/projects/InfoPhys/P1040416-crop.mp4" type="video/mp4" />
  </video>
<div class="cite">
<b>Christian Frisson</b>, Bruno Dumas. “<a href="https://doi.org/10.1145/2839462.2856545">InfoPhys: Direct Manipulation of Information Visualisation Through a Force-Feedback Pointing Device.</a>” ACM TEI’16 WIP (10th Intl. Conf. on Tangible, Embedded, and Embodied Interaction).
</div>

</div>
</div>

<!--slide-->

## Étendons ces domaines de recherche

<!-- <img src="images/venn.svg"/> -->

<svg width="432.53268" height="405.2327">
   <g class="fragment" data-fragment-index="1">
      <circle style="fill:none;stroke:#1f78b4;stroke-width:5" id="path910-9" cx="216.81494" cy="142.36723" r="140" />
      <text x="155" y="64" font-size="55%">Création</text>
      <text x="150" y="84" font-size="55%">d'immersions</text>
   </g>
   <g class="fragment" data-fragment-index="2">
      <circle style="fill:none;stroke:#a6cee3;stroke-width:5" id="path910" cx="142.3401" cy="262.86545" r="140" />
      <text x="15" y="285" font-size="55%">Multimedia</text>
      <text x="15" y="305" font-size="55%">(Mulsemedia)</text>
   </g>
   <g class="fragment" data-fragment-index="3">
      <circle style="fill:none;stroke:#b2df8a;stroke-width:5" id="path910-6" cx="290.19257" cy="262.48398" r="140" />
      <text x="295" y="285" font-size="55%">Collaboratives</text>
      <text x="295" y="305" font-size="55%">(Haptique)</text>
   </g>
</svg>

<!--slide-->

## Atelier Mulsemedia

<!--slide-->

## Mulsemedia?

<!--slide-->

## Mulsemedia = Multisensoriel + Multimedia

<div class="figures">
<div class="figure">
<img class="thumb" alt="" src="images/3rdparty/mulsemedia/9-Figure2-1.png">
<div class="cite">Production-distribution-rendering workflow for creating multisensory systems. 
<br/>
AV = Audio Video, AVHOT = Audio Video Haptic Olfaction Taste. 
<br/>
A. Covaci, Longhao Zou, Irina Tal, Gabriel-Miro Muntean, G. Ghinea
<a href="https://dl.acm.org/doi/pdf/10.1145/3233774">Is Multimedia Multisensorial? - A Review of Mulsemedia Systems</a>
ACM Comput. Surv. 2019
</div>
</div>
</div>


<!--slide-->

## Mulsemedia: enjeux et opportunités

<div class="figures">
<div class="figure">
<img class="thumb" alt="" src="images/3rdparty/mulsemedia/3-Figure1-1.png">
<div class="cite">
The main directions of the survey
<br/>
A. Covaci, Longhao Zou, Irina Tal, Gabriel-Miro Muntean, G. Ghinea
<a href="https://dl.acm.org/doi/pdf/10.1145/3233774">Is Multimedia Multisensorial? - A Review of Mulsemedia Systems</a>
ACM Comput. Surv. 2019
</div>
</div>
</div>

<!--slide-->

## Interaction multimodale


<div class="figures">
<div class="figure">
<img class="thumb" src="images/projects/2006-ACROE-GENESIS.jpg">
<div class="cite">Physical Sound Synthesis <br/>(<a href="http://www.acroe-ica.org/fr/produits/logiciels-applicatifs/genesis">ACROE GENESIS</a>)</div>
</div>
<div class="figure">
<img class="thumb" src="images/other/2007-ACROE-Ergos-Manipulators.jpg">
<div class="cite">Haptic Force Feedback<br/>(<a href="https://web.archive.org/web/20110129080753/http://acroe.imag.fr/ergos-technologies/index.php?idp=0">Ergos TGR</a>)</div>
</div>
</div>


Note: But what if we want to map data to other modalities than vision? ACROE, nemesis of IRCAM in France, have worked for decades on multimodal interaction and simulation for multimedia creation. I got inspired by their work ever since my masters with them. But using their tools brings challenges: not open source, closed distribution.



<!--slide-->

## Simulation audio/haptique

<div class="figures">
<div class="figure">
 <video width="100%" height="auto" controls>
  <source alt="" src="videos/3rdparty/GENESIS - RealTime (RT)-mBKuhlsfsMs.mp4" type="video/mp4">
Your browser does not support the video tag.
</video> 
<div class="cite"></div>
</div>
</div>

<!--slide-->

## Haptique: Experiencer

<!-- ### Opportunités -->

<div class="row">
<div class="col-75">

<div class="figure">

<svg width="100%" height="425px">
<image x="0" y="0" width="100%" height="400px" xlink:href="images/projects/2020-03-05-IDMIL-Haptic-Showroom.jpg"></image>
<g class="fragment" data-fragment-index="1">
<rect x="2%" y="52%" width="23%" height="39%" fill="red" opacity="0.5"/>
<text x="2%" y="43%" width="45%" height="40%" fill="red" font-size="30%">ACROE Ergos TGR (1997-2007)</text>
<g/>
<!-- <g class="fragment"  data-fragment-index="2">
<rect x="26%" y="20%" width="13%" height="29%" fill="orange" opacity="0.5"/>
<rect x="31%" y="73%" width="11%" height="18%" fill="orange" opacity="0.5"/>
<text x="26%" y="19%" width="45%" height="40%" fill="orange" font-size="30%">MBP Freedom 6S (...)</text>
<g/> -->
<g class="fragment"  data-fragment-index="2">
<rect x="76%" y="34%" width="8%" height="10%" fill="orange" opacity="0.5"/>
<text x="74%" y="33%" width="10%" height="10%" fill="orange" font-size="30%">Novint Falcon (2007-201x)</text>
</g>
<g class="fragment" data-fragment-index="3">
<rect x="64%" y="40%" width="5%" height="4%" fill="green" opacity="0.5"/>
<text x="52%" y="26%" width="45%" height="40%" fill="green" font-size="30%">Haply 2-DOF (2018-2020)</text>
<g/>
</svg>


<!-- <img class="thumb" alt="InfoPhys Poster" src="images/projects/2020-03-05-IDMIL-Haptic-Showroom.jpg"> -->
<div class="cite">
Musée Haptique <a href="http://idmil.org" title="Input Devices and Music Interaction Lab">IDMIL</a> (McGill/CIRMMT), agencé et photographié par <b>Christian Frisson</b>, 2020
</div>
</div>

</div>
<div class="col-18">


<p style="font-size: 0.3em;" class="fragment" data-fragment-index="1">
<span style="color: red;">
<!-- Master INPG/ACROE (2006) -->
ACROE Ergos TGR (1997-2007)
</span>

<img class="thumb" alt="InfoPhys Poster" src="images/3rdparty/GENESIS - RealTime (RT)-mBKuhlsfsMs-493.png">

<!-- <br/>
CIRMMT IAM'19 -->

</p>

<p style="font-size: 0.3em;" class="fragment" data-fragment-index="2">
<span style="color: orange;">
<!-- Doctorat UMONS (2010-2015) -->
Novint Falcon (2007-201x)
</span>

<img class="thumb" alt="InfoPhys Poster" src="videos/projects/InfoPhys/P1040416s554.png">

<!-- <br/>
DeviceCycle [NIME'10]
<br/>
Digital Haystack [ACM TEI'14]
<br/>
InfoPhys [ACM TEI'16 WIP] -->
</p>

<p style="font-size: 0.3em;" class="fragment" data-fragment-index="3">
<span style="color: green;">
<!-- Postdoc McGill Mitacs Élévation (2019-2020) -->
Haply 2-DOF (2018-2020)
</span>
<!-- <br/>
FreesoundTracker [HAID'19] -->


<img class="thumb" alt="InfoPhys Poster" src="images/projects/2020-03-05-IDMIL-Haptic-Showroom-Closeup.jpg">

</p>

<!-- <div class="fragment" data-fragment-index="4"> -->
</div>

</div>




<!--slide-->

## Haptique: Enjeux

<!-- ### Enjeux -->

<div class="row">
<div class="col-75">   

<div class="figure">

<svg width="100%" height="425px">
<image x="0" y="0" width="100%" height="400px" xlink:href="images/projects/2020-03-05-IDMIL-Haptic-Showroom.jpg"/></image>
<g class="fragment" data-fragment-index="1">
<rect x="2%" y="52%" width="23%" height="39%" fill="red" opacity="0.5"/>
<text x="2%" y="43%" width="45%" height="40%" fill="red" font-size="30%">ACROE Ergos TGR (1997-2007)</text>
<g/>
<!-- <g class="fragment"  data-fragment-index="2">
<rect x="26%" y="20%" width="13%" height="29%" fill="orange" opacity="0.5"/>
<rect x="31%" y="73%" width="11%" height="18%" fill="orange" opacity="0.5"/>
<text x="26%" y="19%" width="45%" height="40%" fill="orange" font-size="30%">MBP Freedom 6S (...)</text>
<g/> -->
<g class="fragment"  data-fragment-index="2">
<rect x="76%" y="34%" width="8%" height="10%" fill="orange" opacity="0.5"/>
<text x="74%" y="33%" width="10%" height="10%" fill="orange" font-size="30%">Novint Falcon (2007-201x)</text>
</g>
<g class="fragment" data-fragment-index="3">
<rect x="64%" y="40%" width="5%" height="4%" fill="green" opacity="0.5"/>
<text x="52%" y="26%" width="45%" height="40%" fill="green" font-size="30%">Haply 2-DOF (2018-2020)</text>
<g/>
</svg>

<!-- <img class="thumb" alt="InfoPhys Poster" src="images/projects/2020-03-05-IDMIL-Haptic-Showroom.jpg"/> -->
<div class="cite">
Musée Haptique <a href="http://idmil.org" title="Input Devices and Music Interaction Lab">IDMIL</a> (McGill/CIRMMT), agencé et photographié par <b>Christian Frisson</b>, 2020
</div>
</div>

</div>
<div class="col-25">

<!-- ####  -->

<div class="fragment" data-fragment-index="5">

##### Abordable?

<p style="font-size: 0.3em" >
<!-- class="fragment" data-fragment-index="6"> -->
Prix (CAN $):<br/>
<span style="color: red">x (50k)</span>
<span style="color: orange">✔  (250)</span>
<span style="color: green">✔ (150)</span>
<br/>
<i>Mais plus fabriqués!</i>
</p>
</div>
<div class="fragment" data-fragment-index="7">

##### Modulaire?
<p style="font-size: 0.3em" >
<!-- class="fragment" data-fragment-index="8"> -->
Degrés de liberté:<br/>
<span style="color: red">✔ (1-12)</span>
<span style="color: orange">x (3)</span>
<span style="color: green">✔ (1-4)</span>
<br/>
<i>Mais requierent des diplômes en génie mécanique et électrique!</i>
</p>

</div>
<div class="fragment" data-fragment-index="9">

##### Ouvert?

<p style="font-size: 0.3em" >
 <!-- class="fragment" data-fragment-index="10"> -->
Matériels/Logiciels (licences):<br/>  
<span style="color: red">x (©)</span>
<span style="color: orange">~ (©/GPL)</span>
<span style="color: green">✔ (GPL)</span>
<br/>
<i>Mais requierent un diplôme en génie logiciel!</i>
</p>

</div>
<div class="fragment" data-fragment-index="11">

##### Utilisable?

<p style="font-size: 0.3em" >
<!-- class="fragment" data-fragment-index="12"> -->
Auteurisation: <br/>
<span style="color: red">?</span>
<!-- <span data-fragment-index="2" style="color: orange">?</span> -->
<span style="color: orange">?</span>
<span style="color: green">?</span>
<br/>
<i>Reste à explorer et valider!</i>
</p>

</div>

</div>
</div>


<!--section-->

## Présentation

### Outils matériels

(avec kits disponibles en groupes)
   
<div class="row">

<div class="col-50">

#### [LivePose](https://gitlab.com/sat-metalab/livepose/)

> un outil à code source ouvert pour démocratiser la détection de poses et d'action pour les arts 

(15 min)

</div>
<div class="col-50">

#### [ForceHost](https://gitlab.com/ForceHost)

> une solution à code source ouvert pour générer des micrologiciels embarquant l'auteurisation et le rendu audio et haptique à retour de force sur des plateformes ESP32

(15 min)

</div>
</div>

<!--section-->

<!-- .slide: id="livepose" -->

## Présentation
### Outils matériels
#### [LivePose](https://gitlab.com/sat-metalab/livepose/)

> un outil à code source ouvert pour démocratiser la détection de poses et d'action pour les arts 

(15 min)

<!--slide-->
## Comment permettre des interactions de groupe immersives?  

<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/639532760" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

<!--slide-->
## Comment interagir "naturellement" dans les metavers?

<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/604196712" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

<!--slide-->
## Par la détection de poses

<div class="figures">
<div class="figure">
 <video width="100%" height="auto" controls autpplay>
  <source alt="" src="videos/projects/LivePose/openpose_walk_0.mp4" type="video/mp4">
Your browser does not support the video tag.
</video> 
<div class="cite"></div>
</div>
</div>

<!--slide-->
## Architecture

<div class="figures">
<div class="figure">
<img class="thumb" alt="" src="images/projects/LivePose/architecture.png">
<div class="cite"></div>
</div>
</div>

<!--slide-->
## Distribution de dépendances empaquetées

<div class="figures">
<div class="figure">
<img class="thumb" alt="" src="images/projects/LivePose/packages.png">
<div class="cite"></div>
</div>
</div>

<!--slide-->
## Distribution d'un paquet matériel

<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/549423960" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>


<!--slide-->
## Inter-opérabilité: OSSIA

<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/647496450" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>


<!--slide-->
## Inter-opérabilité: Chataigne

<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/639244396" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

<!--slide-->

<!-- .slide: data-background="images/projects/LivePose/LivePoseGitlab.png" data-background-size="60%" -->

<!--section-->

<!-- .slide: id="forcehost" -->

## Présentation

### Outils matériels

#### [ForceHost](https://gitlab.com/ForceHost)

> une solution à code source ouvert pour générer des micrologiciels embarquant l'auteurisation et le rendu audio et haptique à retour de force sur des plateformes ESP32

(15 min)


<!--slide-->


## Présentation

### Outils matériels

#### [ForceHost](https://gitlab.com/ForceHost)

<div class="row">



<div class="col-33 fragment" data-fragment-index="1">

### Torque Tuner 

</div>

<div class="col-33">
</div>

<div class="col-33 fragment" data-fragment-index="2">

### Force Host

</div>


</div>

<div class="row">
<div class="col-33 fragment" data-fragment-index="1">

<img width="80%" src="images/projects/TorqueTuner/TorqueTuner+T-stick.png"/>

NIME 2020

</div>

<div class="col-33 fragment" data-fragment-index="3">

#### Abordable

#### Modulaire

#### Ouvert

#### Utilisable

</div>
<div class="col-33 fragment" data-fragment-index="2">

<!-- <div style="width:100%;"> -->
<img width="80%" src="images/projects/ForceHost/tranfunc-webmapper.png"/>
<!-- </div> -->

NIME 2022


</div>
</div>

Notes:

Je vais vous présenter 2 projets:


<!--slide-->

<!-- Use this to add a background image: -->
<!-- .slide: id="TorqueTuner" data-background="images/projects/TorqueTuner/TorqueTuner+T-stick.png" data-state="TorqueTuner"-->

## Torque Tuner

<!--slide-->

## Module: instrument de musique numérique

<div class="figures">
<div class="figure">
 <video width="100%" height="auto" controls>
  <source alt="" src="videos/projects/TorqueTuner/TorqueTuner + TStick-404592134-00.00.05.676-00.00.19.666-modules.mp4" type="video/mp4">
Your browser does not support the video tag.
</video> 
<div class="cite"></div>
</div>
</div>

<!--slide-->

## Auteurisation par mappage

<div class="figures">
<div class="figure">
 <video width="100%" height="auto">
  <source alt="" src="videos/projects/TorqueTuner/TorqueTuner + TStick-404592134-00.00.29.449-00.00.33.183-webmapper.mp4" type="video/mp4">
Your browser does not support the video tag.
</video> 
<div class="cite">https://github.com/libmapper/webmapper/</div>
</div>
</div>

<!--slide-->

## Exemple: String Plucker: mappage 1

<div class="figures">
<div class="figure">
 <video width="100%" height="auto" controls>
  <source alt="" src="videos/projects/TorqueTuner/TorqueTuner + TStick-404592134-00.02.59.481-00.03.04.016-stringplucker_webmapper_1.mp4" type="video/mp4">
Your browser does not support the video tag.
</video> 
<div class="cite"></div>
</div>
</div>

<!--slide-->

## Exemple: String Plucker: démo 1

<div class="figures">
<div class="figure">
 <video width="100%" height="auto" controls>
  <source alt="" src="videos/projects/TorqueTuner/TorqueTuner + TStick-404592134-00.03.05.883-00.03.11.625-stringplucker_demo_1.mp4" type="video/mp4">
Your browser does not support the video tag.
</video> 
<div class="cite"></div>
</div>
</div>

<!--slide-->

## Exemple: String Plucker: mappage 2

<div class="figures">
<div class="figure">
 <video width="100%" height="auto" controls>
  <source alt="" src="videos/projects/TorqueTuner/TorqueTuner + TStick-404592134-00.03.12.323-00.03.22.389-stringplucker_webmapper_2.mp4" type="video/mp4">
Your browser does not support the video tag.
</video> 
<div class="cite"></div>
</div>
</div>

<!--slide-->

## Exemple: String Plucker: démo 2

<div class="figures">
<div class="figure">
 <video width="100%" height="auto" controls>
  <source alt="" src="videos/projects/TorqueTuner/TorqueTuner + TStick-404592134-00.03.23.116-00.03.40.874-stringplucker_demo_2.mp4" type="video/mp4">
Your browser does not support the video tag.
</video> 
<div class="cite"></div>
</div>
</div>

<!--slide-->

## Exemple: String Plucker: mappage 3

<div class="figures">
<div class="figure">
 <video width="100%" height="auto" controls>
  <source alt="" src="videos/projects/TorqueTuner/TorqueTuner + TStick-404592134-00.03.43.139-00.03.49.918-stringplucker_webmapper_3.mp4" type="video/mp4">
Your browser does not support the video tag.
</video> 
<div class="cite"></div>
</div>
</div>

<!--slide-->

## Exemple: String Plucker: démo 3

<div class="figures">
<div class="figure">
 <video width="100%" height="auto" controls>
  <source alt="" src="videos/projects/TorqueTuner/TorqueTuner + TStick-404592134-00.03.50.216-00.04.20.697-stringplucker_demo_3.mp4" type="video/mp4">
Your browser does not support the video tag.
</video> 
<div class="cite"></div>
</div>
</div>

<!--slide-->

## Exemple: String Plucker: mappage 4

<div class="figures">
<div class="figure">
 <video width="100%" height="auto" controls>
  <source alt="" src="videos/projects/TorqueTuner/TorqueTuner + TStick-404592134-00.04.22.407-00.04.28.149-stringplucker_webmapper_4.mp4" type="video/mp4">
Your browser does not support the video tag.
</video> 
<div class="cite"></div>
</div>
</div>

<!--slide-->

## Exemple: String Plucker: démo 4

<div class="figures">
<div class="figure">
 <video width="100%" height="auto" controls>
  <source alt="" src="videos/projects/TorqueTuner/TorqueTuner + TStick-404592134-00.04.28.199-00.04.40.596-stringplucker_demo_4.mp4" type="video/mp4">
Your browser does not support the video tag.
</video> 
<div class="cite"></div>
</div>
</div>

<!--slide-->
## D'autres exemples sur vimeo

<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/404592134" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

Note: 
* I'd like to show you a recent work from Mathias and Mathias, students at IDMIL, presented at NIME 2020 last Saturday.


<!--slide-->
## Distribution sur github

<div class="figures">
<div class="figure">
<img class="thumb" alt="" src="images/projects/TorqueTuner/TorqueTunerGithub.png">
<div class="cite"></div>
</div>
</div>

<!--slide-->

## Module: molette multimédia

<div class="figures">
<div class="figure">
<img class="thumb" alt="" src="images/projects/TorqueTuner/standalone_knob_merged.png">
<div class="cite"></div>
</div>
</div>


<!--slide-->

<!-- Use this to add a background image: -->
<!-- .slide: id="ForceHost" data-background="images/projects/ForceHost/tranfunc-webmapper-crop.jpg" data-state="ForceHost"-->

## ForceHost

Note: 

ForceHost is an opensource toolchain for generating firmware that hosts authoring and rendering of force-feedback and audio signals and that communicates through I2C with guest motor and sensor boards. With ForceHost, the stability of audio and haptic loops is no longer delegated to and dependent on operating systems and drivers, and devices remain discoverable beyond planned obsolescence. We modified Faust, a high-level language and compiler for real-time audio digital signal processing, to support haptics. Our toolchain compiles audio-haptic firmware applications with Faust and embeds web-based UIs exposing their parameters. We validate our toolchain by example applications and modifications of integrated development environments: Programmation scriptéeexamples of haptic firmwares with our haptic1D Faust library, Programmation visuelle by mapping input and output signals between audio and haptic devices in Webmapper, Programmation visuelle with physically-inspired mass-interaction models in Synth-a-Modeler Designer.


<!--slide-->
## ForceHost dans webmapper

<div class="figures">
<div class="figure">
  <video autoplay loop >
    <source data-src="videos/projects/ForceHost/ForceHostNIME2022-00.00.17.067-00.01.10.806-seg1.mp4" type="video/mp4" />
  </video>
<div class="cite"></div>
</div>
</div>

<!--slide-->
## Temporalités

<div class="figures">
<div class="figure">
<img class="thumb" alt="" src="images/projects/ForceHost/timing.svg">
<div class="cite"></div>
</div>
</div>


<!--slide-->
## Architecture

<div class="figures">
<div class="figure">
<img class="thumb" alt="" src="images/projects/ForceHost/Framework_Blockdiagram_rev4.svg">
<div class="cite"></div>
<p>The ESP32 board communicates via WebSockets with
GUI, via the UDP with Libmapper devices and via a I2C bus with sensors and
actuators</p>
</div>
</div>



<!--slide-->
## Exemple: String Plucker: ForceHost

<div class="figures">
<div class="figure">
  <video>
    <source data-src="videos/projects/ForceHost/ForceHostNIME2022-00.01.12.050-00.02.00.283-seg2.mp4" type="video/mp4" />
  </video>
<div class="cite"></div>
</div>
</div>

<!--slide-->

<!-- .slide: data-background="images/projects/ForceHost/ForceHostGitlab.png" data-background-size="60%" -->


<!--slide-->

## ForceHost: perspectives à la SAT

<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/290925507" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

<!--section-->

## Pause

(10 min)

<!--section-->

## Présentation

### Outils logiciels

<div class="row">

<div class="col-50">

#### [MediaCycle](https://github.com/MediaCycle)

> un cadriciel à code source ouvert pour organiser, visualiser et interagir avec des collections multimedia par similarité

(15 min)

</div>
<div class="col-50">


#### [libmapper / webmapper](https://github.com/libmapper/)

> un cadriciel à code source ouvert (librairie et Web UI) pour connecter des outils, incluant ceux présentés, par le mappage de signaux multimodaux

(15 min)

</div>
</div>

<!--section-->

## Présentation

### Outils logiciels

#### [MediaCycle](https://github.com/MediaCycle)

> un cadriciel à code source ouvert pour organiser, visualiser et interagir avec des collections multimedia par similarité

(15 min)


<!--slide-->

<!-- .slide: data-background-video="videos/MediaCyCle-00.00.09.363-00.00.54.949.mp4"-->

<!--slide-->

## Comment organiser, visualiser et interagir avec des collections multimedia par similarité?

<div class="figures">
<div class="figure">
<img class="thumb" src="images/projects/AudioMetro.png">
<div class="cite">Visualizations of sound collections: AudioMetro [<a href="https://doi.org/10.5281/zenodo.1417245">ISMIR'14</a> + <a href="https://doi.org/10.1145/2636879.2636880">ACM AM'14</a>]</div>
</div>

Note: We also designed visualizations to organize sounds by content-based similarity. On the left you can see file browser layouts from the macOS Finder, from top to bottom: rapid serial visual presentation with CoverFlow, a grid of icons, and a list with metadata. On the right you can see our content-based visualizations, from top to bottom: a grid of sounds ordered by alphabetical order as baseline for experiments, same grid with content-based glyphs, and a layout with glyphs positioned by dimension reduction and proximity grid that we named AudioMetro. We presented results of user studies and proximity grid optimization at ACM AudioMostly and ISMIR in 2014.

<!--slide-->

## Visualisation d'informations

### Variables visuelles

<div class="figures">
<div class="figure">
<img class="thumb" width="40%" src="images/projects/VisualVariables.png">
<div class="cite">from: Charles-Eric Dessart, Vivian Genaro Motti, and Jean Vanderdonckt,
    <br /> <i>Animated Transitions between User Interface Views</i>, ACM AVI'12</small>
<br />
originally: Jock Mckinlay, <i>Automatic design of graphical presentations</i>, PhD,
    Stanford, 1986
</div>
</div>
</div>
</div>

Note: How do one design information visualizations? Ome method is to map data categories, including metrics from audio features extracted from sounds, to visual variables. You can see in this graph with precision on the vertical axis and type of data in the horizontal axis that position is a preferred visual variable.

<!--slide-->

## Visualisation d'informations

### Mapping de données

<div class="figures">
<div class="figure">
<img class="thumb" src="images/projects/20180503-DataMappingExercise-LisaTina.jpg">
<div class="cite">Sketch by <a href="https://lisahynes.weebly.com/">Lisa Hynes</a>  &amp; <a href="http://teenah.ca/">Tina Huynh</a> [IEEE InfoVis'18 poster]
<br>Univ. of Calgary (<a href="https://gitlab.cpsc.ucalgary.ca/EnergyVis">EnergyVis</a> Conditions)</div>
</div>
</div>

Note: Information visualization designers often rely on sketching throughout their design process. Here we asked 2 students from UofC to sketch the data mapping for an infovis project. I suggested to get inspired from visual representations of data flow tools that multimedia practitioners use. 

<!--slide-->

## Prototyper des interactions gestuelles

<div class="figures">
<div class="figure">
<!-- <div class="row"> -->
<!-- <div class="col-50"> -->
<img class="thumb" src="images/projects/DeviceCycleNIME2010Top.png">
<!-- </div> -->
<!-- <div class="col-50"> -->
<!-- <img class="thumb" src="images/projects/DeviceCycleNIME2010Bottom.png"> -->
<!-- </div> -->
<!-- </div> -->
<div class="cite">Reusable input devices (trackpad and force-feedback joystick) <a href="https://github.com/ChristianFrisson/DeviceCycle">DeviceCycle</a> [<a href="https://doi.org/10.5281/zenodo.1177771">NIME'10</a>]</div>
</div>
</div>

Note: I started by gathering my own tools. Off-the-shelf input devices, opensource environments. Here for prototyping interaction with: trackpads and force-feedback joysticks.

<!--slide-->

## Prototyper des interactions gestuelles

<div class="figures">
<div class="figure">
<!-- <div class="row"> -->
<!-- <div class="col-50"> -->
<!-- <img class="thumb" src="images/projects/DeviceCycleNIME2010Top.png"> -->
<!-- </div> -->
<!-- <div class="col-50"> -->
<img class="thumb" src="images/projects/DeviceCycleNIME2010Bottom.png">
<!-- </div> -->
<!-- </div> -->
<div class="cite">Reusable input devices (space navigator and shuttle jog/dial) <a href="https://github.com/ChristianFrisson/DeviceCycle">DeviceCycle</a> [<a href="https://doi.org/10.5281/zenodo.1177771">NIME'10</a>]</div>
</div>
</div>

Note: Here for repurposing a space navigator used by CAD designers, and a shuttle jog/dial, used in media editors. Presented at NIME 2010 with applications for content-based sound browsing. Let's watch a video.

<!--slide-->

## Démo d'interactions gestuelles

<div class="figures">
<div class="figure">
 <video width="100%" height="auto" controls>
  <source alt="" src="videos/DeviceCycleCut.mp4" type="video/mp4">
Your browser does not support the video tag.
</video> 
<div class="cite"></div>
</div>
</div>

<!--slide-->

## Interactions gestuelles 

<div class="figures">
<div class="figure">
<img class="thumb" src="images/projects/FabienGrisardGestureMapping.jpg">
<div class="cite"><a href="http://staff.umons.ac.be/fabien.grisard/">Fabien Grisard</a>: MashtaCycle [<a href="https://doi.org/10.1007/978-3-319-03892-6_14">EAI INTETAIN'13 Springer LNCS</a>]</div>
</div>
</div>

Note: What if we want to design content-based collaborative installations or performances not behind a laptop screen? Fabien Grisard, at the time student at UMONS numediart, now research engineer there, got inspired by sound painting to map free-form gestures to content-based browsing and manipulation. Let's watch a video of Belgian artist Gauthier Keyaerts using this tool. 

<!--slide-->

<!-- .slide: id="MashtaCycleVideo" data-background-video="videos/MashtaCycleCut.mp4" data-state="MashtaCycleVideo"-->

<!--slide-->

<!-- .slide: data-background-color="#999"-->

## Interactions gestuelles 

<div class="figures">
<div class="figure">
<img class="thumb" src="images/projects/MashtaCycleCutEnd.png">
<div class="cite">MashtaCycle [<a href="https://doi.org/10.1007/978-3-319-03892-6_14">EAI INTETAIN'13 Springer LNCS</a>]</div>
</div>
</div>

Note: What if we prefer manipulating instruments with our hands? 
Here is the data flow of the MashtaCycle tool. 
* FUBI: free-form sound painting
* TUIO for multitouch browsing
* MIDI motorized fader box for applying sound effects (RtAudio and RtSynth)

<!--slide-->

## Interaction tangible / avec retour d'effort


<div class="figures">
<div class="figure">
<img class="thumb" src="images/projects/TangibleNeedle.jpg">
<div class="cite">Haptic/Tangible Interactions with Sounds/Videos: Tangible Needle Digital Haystack [<a href="https://doi.org/10.1145/2540930.2540983">ACM TEI'14</a>]</div>
</div>
</div>

Note: 
* Tangible vs Haptic: passive vs active/dynamic feedback on the sense of touch, tactile and kinesthetic
* Intra-video vs inter-audio
* Let's see a short video of this demo presented at ACM TEI 2014.

<!--slide-->

<!-- .slide: data-background-video="videos/TangibleNeedleDigitalHaystack@ACMTEI14.mp4"-->



<!--slide-->

## Interaction haptique et audio

<div class="figures">
<div class="figure">
<img class="thumb" src="images/projects/FreesoundTrackerDemoTable.jpg"/>
<div class="cite">Haptification of Sound Collections <a href="https://github.com/idmil/freesound-tracker">FreesoundTracker</a> [<a href="https://hal.inria.fr/hal-02050235">HAID'19</a>]</div>
</div>
</div>

Note:
* MediaCycle, the framework for content-based multimedia browsing that I contributed to develop during my PhD thesis, is not opensource, so I had to reinvent the wheel to pursue similar research.
* I am currently studying how we can bind sound features to haptic feedback.
* Forked Freesound Explorer a content-based and web-based browser for browsing freesound collections by Frederic Font et al. from UPF. I embedded it in a web-browser application (with web engine Electron), to enable to map input events from haptics devices, not standardized in the web browser.
* I use these openhardware devices made by Haply from Montreal. 2DOF pantographs designed in 1990's at McGill by Vincent Hayward and Christoph Rammstein now at Immersion.
* Presented a demo at the HAID conference in 2019 at Inria, France

<!--slide-->

<!-- .slide: data-background="images/projects/FreesoundTrackerKnownItemSearchCrop.png" data-background-size="60%" data-background-color="#000" -->

Note: 
* Closeup of Freesound Tracker to expose what haptic properties one can bind to each sound.
* I am now examining how to extract material properties from sounds, or synthesize sounds with material properties, to study audio-haptic mappings.

<!--slide-->

<!-- .slide: data-background="images/projects/MediaCycle/MediaCycleGithub.png" data-background-size="60%" data-background-color="#000" -->

<!--slide-->

## MediaCycle: perspectives à la SAT

<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/375715150" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

<!--section-->

## Présentation

### Outils logiciels

#### [libmapper / webmapper](https://github.com/libmapper/)

>  un cadriciel à code source ouvert (librairie et Web UI) pour connecter des outils, incluant ceux présentés, par le mappage de signaux multimodaux

(15 min)

<!--slide-->

## [libmapper / webmapper](https://github.com/libmapper/)

<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/57489444" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

<!--slide-->

## webmapper

### Vues et représentations

<div class="figures">
<div class="figure">
<img class="thumb" alt="" src="images/projects/webmapper/view_selector.png">
<div class="cite"></div>
</div>
</div>

<!--slide-->

## webmapper

### Édition de courbes

<div class="figures">
<div class="figure">
<img class="thumb" alt="" src="images/projects/webmapper/curve_start.png">
<div class="cite"></div>
</div>
<div class="figure">
<img class="thumb" alt="" src="images/projects/webmapper/curve_edit.png">
<div class="cite"></div>
</div>
</div>

<!--slide-->

## webmapper

### Édition d'expressions

<div class="figures">
<div class="figure">
<img class="thumb" alt="" src="images/projects/webmapper/expression_editor.png">
<div class="cite"></div>
</div>
</div>

<!--slide-->

<!-- .slide: data-background="images/projects/webmapper/libmapper-github.png" data-background-size="60%" data-background-color="#000" -->


<!--section-->

## Pause

(10 min)

<!--section-->

## Présentation

### Exploration des démos 

(20 min)


<!--section-->
## Fin

<!--section-->

## Idéation

### Avec fournitures de bureau 

crayons, papier, tableaux, feutres... (ensemble en groupes)

(20 min)

<!--section-->

## Présentation

### Partage des idées et re-groupement

(20 min)

<!--section-->

## Pause

(10 min)

<!--section-->

## Création

### Avec outils matériels et logiciels 

(ensemble en groupes)

(40 min)
